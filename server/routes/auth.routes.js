const express = require('express')

const router = express.Router();


router.post('/register',async() => {

    res.status(201).send({
        message : "register"
    })
})

router.post('/login',async() => {

    res.status(200).send({
        message : "login"
    })
})

router.delete('/logout',async() => {

    res.status(200).send({
        message : "logout"
    })
})

router.post('/refresh',async() => {

    res.status(200).send({
        message : "refresh"
    })
})