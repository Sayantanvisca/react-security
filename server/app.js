const express = require('express');
const morgan = require('morgan');

const app = express();

const middlewares = [
    morgan("dev"),
    express.json()
]

app.use(middlewares)


app.listen(3000,() => {
    console.log("Live on 4000");
})