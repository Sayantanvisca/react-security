import React, { useState } from 'react'
import styled from 'styled-components'

const Login = () => {
    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');

    const onSubmit = eve => {
        eve.preventDefault();
    }

    return (
        <Form onSubmit={onSubmit}>
            <input type="email" name="email" value={email} onChange={eve => setEmail(eve.target.value)} />
            <input type="password" name="password" value={password} onChange={eve => setPassword(eve.target.value)} />
            <button> login </button>
        </Form>
    )
}

export default Login


const Form = styled.form`
display : flex;
gap: 2rem;
flex-direction: column;
`