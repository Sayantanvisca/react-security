import React, { useState } from 'react'
import styled from 'styled-components'

const Register = () => {

    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ name, setName ] = useState('');

    const onSubmit = eve => {
        eve.preventDefault();
    }

    return (
        <Form onSubmit={onSubmit}>
            <input type="text" name="name" value={name} onChange={eve => setName(eve.target.value)} />
            <input type="email" name="email" value={email} onChange={eve => setEmail(eve.target.value)} />
            <input type="password" name="password" value={password} onChange={eve => setPassword(eve.target.value)} />
            <button> register </button>
        </Form>
    )
}

export default Register

const Form = styled.form`
display : flex;
flex-direction: column;
gap: 2rem;
`