import React,{ lazy } from 'react';
import {  BrowserRouter as Router, Switch, Route, NavLink } from 'react-router-dom'
import styled from 'styled-components';

const Home = lazy(() => import('./pages/Home'))
const Login = lazy(() => import('./pages/Login'))
const Register = lazy(() => import('./pages/Register'))

function App() {
  return (
    <Router>
      <Navbar>
        <NavLink to="/">
          Home
        </NavLink>
        <NavLink to="/login">
          Login
        </NavLink>
        <NavLink to="/register">
          Register
        </NavLink>
      </Navbar>
      <Switch>
         <React.Suspense fallback="Loading...">
            <Route path="/" component={Home}/>
            <Route path="/login" component={Login}/>
            <Route path="/register" component={Register}/>
         </React.Suspense>
      </Switch>
    </Router>
  );
}

export default App;


const Navbar = styled.nav`
display: flex;
gap : 2;
padding : 1rem;
`