import { useState } from "react";


const useInput = (state) => {
    const [ inp, setInput ] = useState(state);

    return [inp, (eve) => {
        const { value } = eve.target;

        setInput(value)
    }]
}

export default useInput;